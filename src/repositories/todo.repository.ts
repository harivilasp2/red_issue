import {DefaultKeyValueRepository, juggler} from '@loopback/repository';
import {Todo} from '../models';
import {ReddbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TodoRepository extends DefaultKeyValueRepository<
  Todo
> {
  constructor(
    @inject('datasources.reddb') dataSource: ReddbDataSource,
  ) {
    super(Todo, dataSource);
  }
}
